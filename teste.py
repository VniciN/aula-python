qtd_produto = float(input("Digite a quantidade de produtos: "));
valor_produto = float(input("Digite o valor do produto: "));

total = qtd_produto * valor_produto;

if total < 100: 
    print("O total da compra é: R$", total);
elif total >= 100 and total < 200:
    desconto = total * 0.1;
    total = total - desconto;
    print("O total da compra é: R$", total);
elif total >= 200 and total < 300:
    desconto = total * 0.2;
    total = total - desconto;
    print("O total da compra é: R$", total);
elif total >= 300 and total <= 400:
    desconto = total * 0.3;
    total = total - desconto;
    print("O total da compra é: R$", total);
else: 
    desconto = total * 0.4;
    total = total - desconto;